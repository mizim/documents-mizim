# Mizim City

Mizim is a private, virtual organization on the guld filesystem. It is governed using PGP identities and signatures according to the Guld Membership Agreement.

Physical and legal infrastructure of Mizim are managed by the Mizim Foundation, a Panama Private Interest Foundation.

### Mission

Compile, research, and share knowledge and techniques to improve collaboration and mutual understanding for all thinking beings.

### Summary

A private, virtual community of the Mizim family and adopted friends. The community
meets socially online, sharing images, videos, and other private
documents with complete safety. The virtual city will also have an
economy, law, and government.

Mizim also has a greater purpose, assisting the decentralized guld
community in compiling, codifying and expanding the library of free and
open knowledge known as the guld blocktree. As a family network, Mizim is a trust and
privacy sandbox that doesn't exist naturally online.

It is the sharing of knowledge and techniques that created guld and Mizim together.
Mizim received an endowment of 500,000 guld, as part of the founding and early funding of that digital currency by Mizim members.
This endowment is partially to reflect the various
contributions and support members have made directly or
indirectly to making guld possible. Additionally, this endowment
represents a contract to fulfill the Mizim Mission Statement of sharing
knowledge; any recipients of a Mizim grant or subsidy to do research, develop,
or create some work, must publish said work under Creative Commons,
MIT, or similar license.

